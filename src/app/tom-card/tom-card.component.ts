import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tom-card',
  templateUrl: './tom-card.component.html',
  styleUrls: ['./tom-card.component.css']
})
export class TomCardComponent implements OnInit {
  @Input()
  public name: String;
  @Input()
  public desc: String;
  @Input()
  public icon: String;

  constructor() { }

  ngOnInit(): void {
  }

  onClick(name: String) {
    console.log(name);
  }
}
