import { Component } from '@angular/core';
import { HttpService } from './http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'stopit';
  itemslist = [
    {name: "תכולות", icon: "baki", desc: "דברים שלא ידעתם על המערכת"},
    {name: "משהו", icon: "baki", desc: "משהו משהו"},
    {name: "עוד משהו", icon: "baki", desc: "משהו משהו"},
    {name: "לחשד שךדגל", icon: "baki", desc: "משהו משגדשהו"}];

  constructor(private http: HttpService) {}
  kaki() {
    this.http.getSystems().subscribe();
    console.log("kakak");
  }
}
